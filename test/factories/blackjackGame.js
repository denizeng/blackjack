describe('BlackjackApp.Factories.BlackjackGame', function() {

    var game,
        gameConstructor;

    beforeEach(module('BlackjackApp'));
    beforeEach(module('BlackjackApp.Controllers'));
    beforeEach(module('BlackjackApp.Factories'));

    beforeEach(inject(function($injector,_BlackjackGame_) {
        gameConstructor = _BlackjackGame_;
        game = new _BlackjackGame_(4);
    }));

    it('Should create 4 players and 1 dealer', function() {
        game.startGame();

        var players = game.getPlayers();

        expect(players.length).toEqual(5);
        expect(players[0].isDealer).toEqual(true);
        expect(players[1].isDealer).toEqual(false);
    });

    it('Should give 2 cards to each player including dealer', function() {
        game.startGame();

        var players = game.getPlayers();

        for (var i = 0; i < players.length; i++) {
            expect (players[i].getCards().length).toEqual(2);
        }
    });

    it('Should throw if player count is not between 2 and 25 inclusive', function() {
       expect(function () { new gameConstructor(1); }).toThrow();
       expect(function () { new gameConstructor(26); }).toThrow();
    });

    it('Should not throw if player count is between 2 and 25', function() {
        expect(function () { new gameConstructor(2); }).not.toThrow();
        expect(function () { new gameConstructor(25); }).not.toThrow();
    });
});