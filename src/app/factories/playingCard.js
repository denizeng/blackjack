angular.module('BlackjackApp.Factories').factory(
    "PlayingCard",
    function() {

        // Using https://github.com/coolaj86/knuth-shuffle
        var shuffleDeck = function (array) {
            var currentIndex = array.length, temporaryValue, randomIndex;

            while (0 !== currentIndex) {
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        };

        var getSpecialCardNameAndValues = function (order) {
            var nameAndValues = {};
            if (order === 1) {
                nameAndValues = {name:'Ace of ' + this.suite, value: 11, alternateValue:1};
            } else if (order === 11) {
                nameAndValues = {name:'Jack of ' + this.suite, value:10};
            } else if (order === 12) {
                nameAndValues = {name:'Queen of ' + this.suite, value:10};
            } else if (order === 13) {
                nameAndValues = {name:'King of ' + this.suite, value:10};
            }

            return nameAndValues;
        };

        var setCardNameAndValues = function () {
            if (this.order === 1 || this.order > 10) {
                var nameAndValues = getSpecialCardNameAndValues.apply(this, [this.order]);
                this.name = nameAndValues.name;
                this.value = nameAndValues.value;
                this.alternateValue = nameAndValues.alternateValue;
            } else {
                this.name = this.order + " of " + this.suite;
                this.value = this.order;
            }
        };

        function PlayingCard(order, suite) {
            this.order = order;
            this.suite = suite;

            setCardNameAndValues.apply(this);
        }

        PlayingCard.prototype = {
            getName: function() {
                return( this.name );
            },

            getSuite: function() {
                return( this.suite );
            },

            getValue: function() {
                return( this.value );
            },

            getAlternateValue: function() {
                return( this.alternateValue );
            },

            swapValues: function() {
                var oldValue = this.value;
                this.value = this.alternateValue;
                this.alternateValue = oldValue;
            },

            getOrder: function () {
                return this.order;
            },

            getOrderForRendering: function () {
                return this.order === 1 ? 14 : this.order;
            }
        };

        // Static classes
        PlayingCard.getShuffledStandard52deck = function() {
                var cardSuits = ['Clubs', 'Diamonds', 'Hearts', 'Spades'],
                    deckOfCards = [];

                for (var suiteIndex = 0; suiteIndex < cardSuits.length; suiteIndex++) {
                    for (var cardOrder = 1; cardOrder < 14; cardOrder++) {
                        deckOfCards.push(new PlayingCard(cardOrder, cardSuits[suiteIndex]));
                    }
                }

                return shuffleDeck(deckOfCards);
         };

        return( PlayingCard );
    }
);