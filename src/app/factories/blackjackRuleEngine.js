angular.module('BlackjackApp.Factories').factory(
    "BlackjackRuleEngine", ['BlackjackPlayer', 'PlayingCard', function (BlackjackPlayer, PlayingCard) {

        var self;

        var createGameResult = function (gameStatus, endResultType) {
            return {
                status: gameStatus,
                resultType: endResultType
            };
        };

        var getHighestNumberPlayers = function (activeOrStandingPlayers) {
            var highestNumber = 0,
                highestNumberPlayers = [],
                playerIndex;

            for (playerIndex = 0; playerIndex < activeOrStandingPlayers.length; playerIndex++) {
                if (activeOrStandingPlayers[playerIndex].getCardsTotalValue() >= highestNumber) {
                    if (activeOrStandingPlayers[playerIndex].getCardsTotalValue() > highestNumber) {
                        highestNumberPlayers = [];
                        highestNumberPlayers.push(activeOrStandingPlayers[playerIndex]);
                        highestNumber = activeOrStandingPlayers[playerIndex].getCardsTotalValue();
                    } else {
                        highestNumberPlayers.push(activeOrStandingPlayers[playerIndex]);
                    }
                }
            }

            return  {
                highestNumber: highestNumber,
                highestNumberPlayers: highestNumberPlayers
            };
        };

        var getActiveStandingAcePlayers = function () {
            var playerIndex,
                playerStatus,
                player,
                activeOrStandingPlayers = [],
                acePlayers = [];

            for (playerIndex = 1; playerIndex < this.blackjackPlayers.length; playerIndex++) {
                player = this.blackjackPlayers[playerIndex];
                playerStatus = player.getStatus();

                if (playerStatus ===  BlackjackPlayer.Status.ACTIVE || playerStatus ===  BlackjackPlayer.Status.STANDING) {
                    activeOrStandingPlayers.push(player);
                } else if (playerStatus ===  BlackjackPlayer.Status.ACE) {
                    acePlayers.push(player);
                }
            }

            return {
                activeOrStandingPlayers: activeOrStandingPlayers,
                acePlayers: acePlayers
            };
        };

        var dealerPlayUntilEnd = function (dealer, _dealerTotal, highestNumber) {
            var dealerStatus,
                dealerTotal = _dealerTotal;

            while (this.deckOfCards.length > 0 && dealerTotal < highestNumber && dealerStatus !== BlackjackPlayer.Status.ACE && dealerStatus !== BlackjackPlayer.Status.BUST) {
                // hit until bust or ace,
                dealer.playTurn();
                dealerStatus = dealer.getStatus();
                dealerTotal = dealer.getCardsTotalValue();
            }

            return dealer.getStatus();
        };

        var handleGameEnd = function () {

            var activePlayers = getActiveStandingAcePlayers.apply(this),
                activeOrStandingPlayers = activePlayers.activeOrStandingPlayers,
                acePlayers = activePlayers.acePlayers,
                dealer = this.blackjackPlayers[0],
                dealerTotal = dealer.getCardsTotalValue(),
                dealerStatus = dealer.getStatus();

            if (dealerTotal === 21) {
                console.log('Dealer won by Blackjack');
                this.gameResult = createGameResult(BlackjackRuleEngine.States.ENDED,
                    BlackjackRuleEngine.EndResultTypes.DEALERBLACKJACKWIN);
            } else if (acePlayers.length > 0) {
                    this.gameResult = createGameResult(BlackjackRuleEngine.States.ENDED,
                        BlackjackRuleEngine.EndResultTypes.OTHERBLACKJACKWIN);
            } else if (activeOrStandingPlayers.length === 0) {
                console.log('Dealer won because all players are bust.');
                this.gameResult = createGameResult(BlackjackRuleEngine.States.ENDED,
                    BlackjackRuleEngine.EndResultTypes.ALLBUSTDEALERWIN);
            }
            else {
                var highestNumberResult = getHighestNumberPlayers(activeOrStandingPlayers);

                var highestNumber = highestNumberResult.highestNumber,
                    highestNumberPlayers = highestNumberResult.highestNumberPlayers;

                dealerStatus = dealerPlayUntilEnd.apply(this, [dealer, dealerTotal, highestNumber]);
                dealerTotal = dealer.getCardsTotalValue();

                if (dealerStatus === BlackjackPlayer.Status.BUST || dealerTotal < highestNumber) {
                    console.log('Other win by greater number!', highestNumberPlayers.length);
                    this.gameResult = createGameResult(BlackjackRuleEngine.States.ENDED,
                        BlackjackRuleEngine.EndResultTypes.OTHERSWINBYTOTAL);
                } else if (dealerTotal === 21) {
                    console.log('Dealer won by Blackjack');
                    this.gameResult = createGameResult(BlackjackRuleEngine.States.ENDED,
                        BlackjackRuleEngine.EndResultTypes.DEALERBLACKJACKWIN);
                } else {
                    console.log('Dealer wins by greater or equal number!');
                    this.gameResult = createGameResult(BlackjackRuleEngine.States.ENDED,
                        BlackjackRuleEngine.EndResultTypes.DEALERWINBYTOTAL);
                }
            }
        };

        var shouldGameContinue = function () {
            var acePlayers = [],
                player,
                playerTotal = 0,
                activePlayerCount = 0,
                dealerHasAce = this.blackjackPlayers[0].getCardsTotalValue() === 21;

            console.log('Dealer has ace:' + dealerHasAce);
            for (var i = 1; i < this.blackjackPlayers.length; i++) {
                player = this.blackjackPlayers[i];

                if (player.getStatus() === BlackjackPlayer.Status.ACTIVE) {
                    var isStanding = player.getStatus() === BlackjackPlayer.Status.STANDING;
                    playerTotal = player.getCardsTotalValue();
                    if (playerTotal === 21) {
                        acePlayers.push(player);
                        player.setStatus(BlackjackPlayer.Status.ACE);
                    } else if (playerTotal > 21) {
                        player.setStatus(BlackjackPlayer.Status.BUST);
                    } else if (!isStanding) {
                        activePlayerCount++;
                    } else {
                        player.setStatus(BlackjackPlayer.Status.STANDING);
                    }
                }
            }

            // dealing ends for players
            if (acePlayers.length > 0 || activePlayerCount === 0 || this.deckOfCards.length === 0) {
                handleGameEnd.apply(this, [acePlayers]);
                return false;
            }

            return true;
        };

        var getCardFromDeck = function () {
            return this.deckOfCards.pop();
        };

        var startGameLoop = function () {
            // Starts from first player using 0 index (skipping dealer)
            var currentPlayerTurn = 1,
                self = this;

            gameLoop();

            function gameLoop() {
                if (shouldGameContinue.apply(self)) {
                    self.blackjackPlayers[currentPlayerTurn].playTurn().then(function () {

                        console.log('resolved');

                        currentPlayerTurn++;
                        if (currentPlayerTurn === self.blackjackPlayers.length) {
                            currentPlayerTurn = 1;
                        }
                        gameLoop();
                    });
                }
            }
        };

        var playHandler = function (player, action) {
            var status = BlackjackPlayer.Status.ACTIVE;

            if (action === BlackjackPlayer.Actions.HIT) {
                player.cards.push(getCardFromDeck.apply(self));

                var playerTotal = player.getCardsTotalValue();
                if (playerTotal === 21) {
                    status = BlackjackPlayer.Status.ACE;
                } else if (playerTotal > 21) {
                    status = BlackjackPlayer.Status.BUST;
                }
            } else {
                status = BlackjackPlayer.Status.STANDING;
            }

            return status;
        };

        function BlackjackRuleEngine(numberOfPlayers) {

            // REFACTOR: NaN is the only JavaScript value that is treated as unequal to itself
            if (typeof numberOfPlayers !== 'number' || isNaN(numberOfPlayers)|| numberOfPlayers < 2 || numberOfPlayers > 25 ) {
                throw new Error("There should be at least two players, at most 26 players.");
            }

            this.blackjackPlayers = [];

            // push first player as dealer (second constructor argument true)
            var player;

            for (var i =0; i < numberOfPlayers + 1; i++) {
                player = new BlackjackPlayer(i, i === 0 ? true : false, playHandler);
                this.blackjackPlayers.push(player);
            }

            this.deckOfCards = PlayingCard.getShuffledStandard52deck();

            this.gameResult = createGameResult(BlackjackRuleEngine.States.GAMERPLAYING,
                BlackjackRuleEngine.EndResultTypes.INPROGRESS);

            self = this;
        }

        BlackjackRuleEngine.prototype = {
            startGame: function () {
                console.log('Game started');

                // give each player 2 cards
                for (var i = 0; i < this.blackjackPlayers.length; i++) {
                    this.blackjackPlayers[i].cards.push(getCardFromDeck.apply(this));
                    this.blackjackPlayers[i].cards.push(getCardFromDeck.apply(this));
                }

                // give turn to player unless game finished on first dealing of cards
                startGameLoop.apply(this);
            },

            getPlayers: function () {
                return this.blackjackPlayers;
            },

            getResult: function () {
                return this.gameResult;
            }
        };

        BlackjackRuleEngine.States = {
            "GAMERPLAYING": {value: 1, name: "GamerPlaying" },
            "DEALERPLAYING": {value: 2, name: "DealerPlaying" },
            "ENDED": {value: 3, name: "Ended" }
        };

        BlackjackRuleEngine.EndResultTypes = {
            "INPROGRESS": {value: 1, name: "InProgress", description: "Game in progress." },
            "DEALERBLACKJACKWIN": {value: 2, name: "DealerBlackjackWin", description: "Dealer won by Blackjack!" },
            "OTHERBLACKJACKWIN": {value: 3, name: "OtherBlackjackWin", description: "Other players won by Blackjack!" },
            "DEALERLATEBLACKJACKWIN": {value: 4, name: "DealerLateBlackjackWin", description: "Dealer won by late Blackjack!" },
            "ALLBUSTDEALERWIN": {value: 5, name: "AllBustDealerWin", description: "Dealer won because all players are bust!" },
            "OTHERSWINBYTOTAL": {value: 6, name: "OthersWinByTotal", description: "Other players win by greater number!" },
            "DEALERWINBYTOTAL": {value: 7, name: "DealerWinByTotal", description: " Dealer wins by greater or equal number!" }
        };

        return( BlackjackRuleEngine );
}]);