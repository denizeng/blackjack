angular.module('BlackjackApp.Factories').factory(
    "BlackjackGame", ['BlackjackPlayer', 'BlackjackRuleEngine',  function (BlackjackPlayer, BlackjackRuleEngine) {

        var ruleEngine;

        function BlackjackGame(numberOfPlayers) {
            ruleEngine = new BlackjackRuleEngine(numberOfPlayers);
        }

        BlackjackGame.prototype = {

            startGame: function () {
                ruleEngine.startGame();
            },

            getPlayers: function () {
                return ruleEngine.getPlayers();
            },

            getResult: function () {
                return ruleEngine.getResult();
            }
        };

        return( BlackjackGame );
}]);