angular.module('BlackjackApp.Factories').factory(
    "BlackjackPlayer",
    function($q) {

        var playAction = function (action) {
            var status = this.playHandler(this, action);
            console.log('setting status to:' + status);
            this.status = status;
        };

        var getActionByUiParameter = function (actionValue) {
            var action;

            if (actionValue === 1) {
                action = BlackjackPlayer.Actions.STAND;
            } else {
                action = BlackjackPlayer.Actions.HIT;
            }

            return action;
        };

        var play = function () {
            var self = this,
                action,
                deferred = $q.defer();

            if (this.status === BlackjackPlayer.Status.ACTIVE) {
                console.log(this.getPlayerName() + " starting turn");
                this.isPlaying = true;
            } else {
                console.log('Player is done, not playing this turn.');
                deferred.resolve('resolved');
                return deferred.promise;
            }

            if (this.isDealer) {
                action = BlackjackPlayer.Actions.HIT;
                playAction.apply(this, [action]);
                deferred.resolve('resolved');
            } else {
                //player object has a promise, waiting to be resolved in the UI
                this.uiListener = function (actionValue) {
                    action = getActionByUiParameter(actionValue);
                    playAction.apply(self, [action]);
                    self.isPlaying = false;
                    deferred.resolve('resolved');
                };

            }

            return deferred.promise;
        };

        function BlackjackPlayer(order, isDealer, playEventHandler) {
            this.name = "Player " + order;
            this.isDealer = isDealer;
            this.isPlaying = false;
            this.cards = [];
            this.playHandler = playEventHandler;
            this.status = BlackjackPlayer.Status.ACTIVE;
        }

        BlackjackPlayer.prototype = {
            getPlayerName: function () {
                return this.isDealer ? "Dealer" : this.name;
            },

            getCardsTotalValue: function () {
                var sum = 0,
                    card;

                for (var i = 0; i < this.cards.length; i++) {
                    card = this.cards[i];

                    if (card.value === 11)
                    {
                        var newSum = sum + card.getValue();
                        if (newSum > 21) {
                            card.swapValues();
                        }
                    }

                    sum += card.getValue();
                }

                return sum;
            },

            playTurn: function () {
                return play.apply(this);
            },

            getStatus: function () {
                return this.status;
            },

            setStatus: function (status) {
                this.status  = status;
            },

            getCards: function () {
                return this.cards;
            }
        };

        BlackjackPlayer.Status = {
            "ACTIVE": {value: 1, name: "Active" },
            "STANDING": {value: 2, name: "Standing" },
            "BUST": {value: 3, name: "Bust" },
            "ACE": {value: 4, name: "Ace" }
        };

        BlackjackPlayer.Actions = {
            "STAND": {value: 1, name: "Stand" },
            "HIT": {value: 2, name: "Hit" }
        };

        return( BlackjackPlayer );
    }
);