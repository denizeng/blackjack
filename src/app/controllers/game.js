angular.module('BlackjackApp.Controllers').controller('GameController', ['BlackjackGame','$scope', function(blackjackGame, $scope) {

        // Default value, can be changed in UI
        $scope.playerCount = 2;

        $scope.newGame = function (_playerCount) {
            var playerCount = parseInt(_playerCount, 10);

            try {
                $scope.game = new blackjackGame(playerCount);
                $scope.game.startGame();
            } catch (e) {
                alert(e);
            }
        };
 }]);