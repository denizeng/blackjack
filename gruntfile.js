module.exports = function(grunt) {

  grunt.initConfig({
    jshint: {
      files: ['Gruntfile.js',
              'src/**/*.js',
              '!src/lib/**/*.js',
              'test/**/*.js'],
      options: {
        globals: {
          jQuery: true
        }, reporter: require('jshint-stylish')
      }
    },
    watch: {
      files: ['<%= jshint.files %>'],
      tasks: ['jshint']
    },
    karma: {
      unit: {
          configFile: 'karma.conf.js'
        }
    },
    connect: {
      server: {
          options: {
              port: 8080,
              base: 'src',
              keepalive: true
          }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-karma');

  grunt.registerTask('default', ['jshint', 'karma', 'connect']);
};